#include <boarddefs.h>
#include <IRremote.h>
#include <IRremoteInt.h>
#include <ir_Lego_PF_BitStreamEncoder.h>

int RECV_PIN = 11;
int relay = 2;
IRrecv irrecv(RECV_PIN);
decode_results results;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  irrecv.enableIRIn();
  pinMode(relay, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  if(irrecv.decode(&results))
  {
    Serial.println(results.value, HEX);
    if (results.value == 0x80C) {
      Serial.println("Button 1");
      digitalWrite(relay, HIGH);
    } else if(results.value == 0xC) {
      digitalWrite(relay, LOW);
    }
    
    irrecv.resume();  
  }
  
}
